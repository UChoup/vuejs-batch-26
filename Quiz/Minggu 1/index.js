// Soal 1
function jumlah_kata(kalimat) {
    var banyak_kata = kalimat
    banyak_kata = banyak_kata.split(" ")
    console.log(banyak_kata.length);
}

var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok"
var kalimat_2 = "Saya Iqbal"

jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2

// Soal 2
function next_date(tgl, bln, thn) {
    const months = ["0", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    var d = new Date(new Date(thn, bln, tgl).getTime() + 24 * 60 * 60 * 1000);
    var day = d.getDate()
    var month = months[d.getMonth()]
    var year = d.getFullYear()
    console.log(day + " " + month + " " + year);
}


var tanggal = 29
var bulan = 2
var tahun = 2020

next_date(tanggal, bulan, tahun) // output : 1 Maret 2020

// contoh 2

var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal, bulan, tahun) // output : 1 Maret 2021

// contoh 3

var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal, bulan, tahun) // output : 1 Januari 2021
