// Soal 1
var nilai = 75;

if (nilai >= 85) {
    console.log("Indeks : A")
} else if (nilai >= 75 && nilai < 85){
    console.log("Indeks : B")
} else if(nilai >= 65 && nilai < 75){
    console.log("Indeks : C")
} else if (nilai >= 55 && nilai < 65) {
    console.log("Indeks : D")
} else if(nilai < 55){
    console.log("Indeks : E");
}

// Soal 2
var tanggal = 10;
var bulan = 1;
var tahun = 2001;

switch (bulan) {
    case 1:
        console.log(tanggal+" Januari "+tahun);
        break;

    case 2:
        console.log(tanggal + " Februari " + tahun);
        break;

    case 3:
        console.log(tanggal + " Maret " + tahun);
        break;
    
    case 4:
        console.log(tanggal + " April " + tahun);
        break;

    case 5:
        console.log(tanggal + " Mei " + tahun);
        break;

    case 6:
        console.log(tanggal + " Juni " + tahun);
        break;

    case 7:
        console.log(tanggal + " Juli " + tahun);
        break;

    case 8:
        console.log(tanggal + " Agustus " + tahun);
        break;

    case 9:
        console.log(tanggal + " September " + tahun);
        break;

    case 10:
        console.log(tanggal + " Oktober " + tahun);
        break;

    case 11:
        console.log(tanggal + " November " + tahun);
        break;

    case 12:
        console.log(tanggal + " Desember " + tahun);
        break;
}

// Soal 3
var n = 7;
var output = '';
for (var i = 1; i <= n; i++) {
    for (var j = 1; j <= i; j++) {
        output +=  "#";
    }
    console.log(output);
    output = '';
}

// Soal 4
// Perbaikan
var m = 7
for(var i = 1; i <= m; i++){
    j = i%3

    switch (j) {
        case 1:
            console.log(i + " - I Love Programming");
            break;
    
        case 2:
            console.log(i + " - I Love Javascript");
            break;
        
        default:
            console.log(i+ " - I Love VueJS");

            var sama_dengan = ""

            for (var k = 0; k<i; k++){
                sama_dengan = sama_dengan + "="
            }

            console.log(sama_dengan);
            break;
    }
}