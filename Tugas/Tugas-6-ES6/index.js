// Soal 1
const luasPersegiPanjang = (p, l) =>{
    let luas = p*l
    return luas
}

const kelilingPersegiPanjang = (p, l) => {
    let keliling = 2*(p + l)
    return keliling
}

console.log(luasPersegiPanjang(2,5));
console.log(kelilingPersegiPanjang(2, 5));

// Soal 2
const literal = (a, b) => {
    return {
        a, 
        b,

        fullName: () => console.log(`${a} ${b}`)
    }
    
}
literal("william", "imoh").fullName()

// Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)

// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]
console.log(combined)

// Soal 5
const planet = "earth"
const view = "glass"
const after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(after);




    