// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
console.log(daftarHewan[4]);
console.log(daftarHewan[0]);
console.log(daftarHewan[2]);
console.log(daftarHewan[3]);
console.log(daftarHewan[1]);

// Soal 2
function introduce() {
    var jawab = "Nama saya "+data.name+", umur saya "+data.age+" tahun, alamat saya di "+data.address+", dan saya punya hobby yaitu "+data.hobby+"!"
    return jawab
}
var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan)

// Soal 3
function hitung_huruf_vokal(str) {

    var count = str.match(/[aeiou]/gi).length;

    return count;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2) // 3 2

// Soal 4
function hitung(x) {
    return 2*x-2
}

console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(5)) // 8