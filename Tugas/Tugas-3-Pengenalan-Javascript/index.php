<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas Javascript</title>
</head>
<body>
    <h2>Soal 1</h2>
    <div id="soal1"></div>

    <h2>Soal 2</h2>
    <div id="soal2"></div>

    <h2>Soal 3</h2>
    <div id="soal3">di console</div>

    <script src="index.js"></script>
</body>
</html>