var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise 
// Soal 2
const time = 10000

// readBooksPromise(time, books[0])
//     .then(sisawaktu0 => {
//         readBooksPromise(sisawaktu0, books[1])
//             .then(sisawaktu1 => {
//                 readBooksPromise(sisawaktu1, books[2])
//                     .then(sisawaktu2 => {
//                         readBooksPromise(sisawaktu2, books[3])
//                             .then(sisawaktu3 => {
//                                 return sisawaktu3
//                             })
//                     })
//             })
//     })
//     .catch(err => console.log(err))



// Acyn Await
const execute = async () => {
    const sisawaktu0 = await readBooksPromise(time, books[0])
    const sisawaktu1 = await readBooksPromise(sisawaktu0, books[1])
    const sisawaktu2 = await readBooksPromise(sisawaktu1, books[2])
    const sisawaktu3 = await readBooksPromise(sisawaktu2, books[3])

    return console.log('selesai');
}
execute()