export const CounterComponent = {
    template : `
        <div>
            <p>state pada component counter {{counter}}</p>
        </div>
    `,
    computed : {
        counter() {
            return this.$store.getters.counter
        }
    }
}