Vue.component('todo', {
    template: '<div class="container">' +
        '<div class="row justify-content-center">' +
        '<div class="col-md-6">' +
        '<h1 class="text-center">Todolist</h1>' +
        '<hr>' +
        '<input type="text" class="form-control" placeholder="Input todolist & enter" v-model="todo.name" v-on:keyup.enter="addTodo">' +
        '<hr>' +
        '<ul class="list-group">' +
        '<todo-item v-for="item,index in todos" v-bind:item="item" v-bind:key="index" v-on:toggleTodo="toggleTodo" v-on:deleteTodo="deleteTodo"></todo-item>' +
        '</ul>' +
        '</div>' +
        '</div>' +
        '</div>',
    data: function () {
        return {
            todos: [
                {
                    name: 'Todo 1',
                    done: true,
                },
                {
                    name: 'Todo 2',
                    done: false,
                },
                {
                    name: 'Todo 3',
                    done: false,
                },
            ],
            todo: {
                name: '',
                done: false
            }
        }
    },
    methods: {
        addTodo() {
            if (this.todo.name != '') {
                this.todos.push(this.todo)
                this.todo = {
                    name: '',
                    done: false
                }
            }
        },
        toggleTodo(todo) {
            let index = this.todos.indexOf(todo);
            this.todos[index].done = !this.todos[index].done
        },
        deleteTodo(todo) {

            let index = this.todos.indexOf(todo);
            this.todos.splice(index, 1)

        },

    }
});

Vue.component('todo', {
    template: '<div class="container">' +
        '<div class="row justify-content-center">' +
        '<div class="col-md-6">' +
        '<h1 class="text-center">Todolist</h1>' +
        '<hr>' +
        '<input type="text" class="form-control" placeholder="Input todolist & enter" v-model="todo.name" v-on:keyup.enter="addTodo">' +
        '<hr>' +
        '<ul class="list-group">' +
        '<todo-item v-for="item,index in todos" v-bind:item="item" v-bind:key="index" ></todo-item>' +
        '</ul>' +
        '</div>' +
        '</div>' +
        '</div>',
    data: function () {
        return {
            status: 'all',
            todo: {
                name: '',
                done: false
            }
        }
    },
    computed: {
        todos() {
            return this.$store.state.todos
        },
    },
    methods: {
        addTodo() {

        }
    }
});

const store = new Vuex.Store({
    state: {
        todos: [
            {
                name: 'Todo 1',
                done: false,
            },
            {
                name: 'Todo 2',
                done: false,
            },
            {
                name: 'Todo 3',
                done: false,
            },
        ],
    },
    mutations: {

    },
    getters: {

    },
})

var app = new Vue({
    el: '#app',
    store,
    data: {
        'message': 'Hello world'
    },
});